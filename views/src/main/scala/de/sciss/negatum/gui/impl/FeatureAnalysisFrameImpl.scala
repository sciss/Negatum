/*
 *  FeatureAnalysisFrameImpl.scala
 *  (Negatum)
 *
 *  Copyright (c) 2016-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.negatum
package gui
package impl

import de.sciss.lucre.synth
import de.sciss.mellite.UniverseHandler
import de.sciss.mellite.impl.WorkspaceWindowImpl

object FeatureAnalysisFrameImpl {
  def apply[T <: synth.Txn[T]](negatum: Negatum[T])(implicit tx: T,
                                                    handler: UniverseHandler[T]): FeatureAnalysisFrame[T] =
    handler(negatum, FeatureAnalysisFrame) {
      val view = FeatureAnalysisView[T](negatum)
      val res = new Impl[T](view)
      res.init()
      res
    }

  private final class Impl[T <: synth.Txn[T]](val view: FeatureAnalysisView[T])
                                             (implicit val handler: UniverseHandler[T])
    extends FeatureAnalysisFrame[T] with WorkspaceWindowImpl[T] {

    override def key: Key = FeatureAnalysisFrame

    // override this because NegatumObjView will otherwise share the same view state
    override protected def viewStateKey: String = "view-analysis"

    // XXX TODO
    override def supportsNewWindow: Boolean = false

    override def newWindow()(implicit tx: T): Repr[T] = throw new UnsupportedOperationException
  }
}
