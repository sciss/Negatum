/*
 *  NegatumViewImpl.scala
 *  (Negatum)
 *
 *  Copyright (c) 2016-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.negatum.gui.impl

import de.sciss.icons.raphael
import de.sciss.lucre.Txn.{peer => txPeer}
import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.expr.CellView
import de.sciss.lucre.swing.LucreSwing.deferTx
import de.sciss.lucre.swing.impl.ComponentHolder
import de.sciss.lucre.swing.{BooleanCheckBoxView, DoubleSpinnerView, IntSpinnerView, StringFieldView, TargetIcon, View}
import de.sciss.lucre.{BooleanObj, DoubleObj, DoubleVector, Folder, IntObj, Obj, Source, StringObj, synth}
import de.sciss.mellite.{DragAndDrop, FolderView, GUI, ObjView, Prefs, UniverseHandler, ViewState}
import de.sciss.negatum.gui.{FeatureAnalysisFrame, NegatumView}
import de.sciss.negatum.impl.UGens
import de.sciss.negatum.{Negatum, Optimize, Parametrize, Rendering}
import de.sciss.proc.{Code, ParamSpec, Proc, Universe}
import de.sciss.processor.Processor
import de.sciss.swingplus.{ComboBox, GroupPanel, Spinner}

import javax.swing.TransferHandler.TransferSupport
import javax.swing.{JLabel, SpinnerNumberModel, TransferHandler}
import scala.concurrent.ExecutionContext
import scala.concurrent.stm.Ref
import scala.swing.{Alignment, BorderPanel, Button, Component, Dialog, Dimension, FlowPanel, Graphics2D, Label, ProgressBar, Swing}
import scala.util.{Failure, Success}

object NegatumViewImpl {
  def apply[T <: synth.Txn[T]](n: Negatum[T])(implicit tx: T, handler: UniverseHandler[T]): NegatumView[T] = {
    implicit val undo: UndoManager[T] = UndoManager()
    val res = new Impl[T](tx.newHandle(n))
    res.init(n)
  }

  private final class Impl[T <: synth.Txn[T]](negatumH: Source[T, Negatum[T]])
                                             (implicit handler: UniverseHandler[T], val undoManager: UndoManager[T])
    extends NegatumView[T] with ComponentHolder[Component] {

    override type C = Component

    override def viewState: Set[ViewState] = Set.empty  // XXX TODO

    override implicit val universe: Universe[T] = handler.universe

    private[this] val renderRef           = Ref(Option.empty[Rendering[T, Unit]])
    private[this] val optParStackRef      = Ref(List.empty[Source[T, Proc[T]]])
    private[this] val optParProcessorRef  = Ref(Option.empty[Processor[Any]])

    override def obj(implicit tx: T): Obj[T] = negatum

    private def defaultOptAnaDur(n: Negatum[T])(implicit tx: T): Double = {
      val t     = n.template()
      val sr    = t.sampleRate
      math.max(2.0, t.numFrames / sr)
    }

    private lazy val mOptType = ComboBox.Model.wrap(Seq("Optimize", "Parametrize"))

    private val nameOptPar = "Optimize/Parametrize"

    private final class DropProcLabel extends Label {
      override lazy val peer: JLabel = new JLabel(" ") with SuperMixin {
        // XXX TODO --- hack to avoid too narrow buttons under certain look-and-feel
        override def getPreferredSize: Dimension = {
          val d = super.getPreferredSize
          if (!isPreferredSizeSet) {
            val e     = math.max(24, math.max(d.width, d.height))
            d.width   = e
            d.height  = e
          }
          d
        }
      }

      override protected def paintComponent(g: Graphics2D): Unit = {
        super.paintComponent(g)
        val p       = peer
        val w       = p.getWidth
        val h       = p.getHeight
        val extent  = math.min(w, h)
        if (extent > 0) {
          TargetIcon.paint(g, x = (w - extent) >> 1, y = (h - extent) >> 1, extent = extent, enabled = enabled)
        }
      }

      private object TH extends TransferHandler {
        private def runProc(p: Proc[T])(implicit tx: T): Unit = {
          import Negatum._
          val obj   = negatumH()
          val attr  = obj.attr
          val gIn   = p.graph().value
          import de.sciss.proc.Implicits._
          val name  = p.name
          val t     = obj.template()
          val sr    = t.sampleRate
          val bs    = Prefs.audioBlockSize.getOrElse(Prefs.defaultAudioBlockSize)
          val dur   = attr.$[DoubleObj  ](attrOptDuration     ).map(_.value).getOrElse(defaultOptAnaDur(obj))
          val expPr = attr.$[BooleanObj ](attrOptExpandProtect).forall(_.value)
          val expIO = attr.$[BooleanObj ](attrOptExpandIO     ).forall(_.value)
          val mono  = attr.$[BooleanObj ](attrOptMono         ).forall(_.value)
          val maxPar= attr.$[IntObj     ](attrParMaxParams    ).fold(5)(_.value)
          val numCh = attr.$[IntObj     ](attrParNumChannels  ).fold(2)(_.value)

          val processor = if (mOptType.indexOf(mOptType.selectedItem.getOrElse("")) < 1) {
            val cfg = Optimize.Config(
              graph           = gIn,
              sampleRate      = sr,
              blockSize       = bs,
              analysisDur     = dur,
              expandProtect   = expPr,
              expandIO        = expIO
            )
            cfg.mono = mono
            Optimize(cfg)
          } else {
            val cfg = Parametrize.Config(
              graph         = gIn,
              sampleRate    = sr,
              analysisDur   = dur,
              maxParams     = maxPar,
              expandProtect = expPr,
              expandIO      = expIO,
              mono          = mono,
            )
            Parametrize(cfg)
          }
          optParProcessorRef() = Some(processor)
          val procH = tx.newHandle(p: Proc[T])
          import ExecutionContext.Implicits.global
          processor.onComplete {
            case Success(res0) =>
              res0 match {
                case res: Optimize.Result =>
                  println(s"Optimization for '$name' found ${res.numConst} constant replacement and ${res.numEqual} redundant elements.")
                case res: Parametrize.Result =>
                  println(s"Parametrization for '$name' found ${res.specs.size} parameters.")
                case _ => assert(false, res0)
              }
              universe.cursor.step { implicit tx =>
                optParProcessorRef() = None
                val p         = procH()
                val pAttr     = p.attr
                res0 match {
                  case res: Optimize.Result =>
                    p.graph()    = res.graph
                  case res: Parametrize.Result =>
                    p.graph()    = res.graph
                    res.specs.zipWithIndex.foreach { case ((default, spec), si) =>
                      val paramObj: Obj[T] = if (numCh == 1) {
                        DoubleObj.newVar(DoubleObj.newConst[T](default))
                      } else {
                        val defaultN = Vector.fill(numCh)(default)
                        DoubleVector.newVar(DoubleVector.newConst[T](defaultN))
                      }
                      val specObj   = ParamSpec.Obj.newConst[T](spec)
                      val key       = s"p${si + 1}"
                      val specKey   = ParamSpec.composeKey(key)
                      pAttr.put(specKey, specObj)
                      pAttr.put(key, paramObj)
                    }
                  case _ => assert(false, res0)
                }
                pAttr.remove(Code.attrSource) // make sure it's regenerated if the editor is opened
                tryNextProc()
              }

            case Failure(ex) =>
              val cont = ex match {
                case Processor.Aborted() => false
                case _ =>
                  println(s"Optimize/Parametrize for '$name' failed")
                  ex.printStackTrace()
                  true
              }
              universe.cursor.step { implicit tx =>
                optParProcessorRef() = None
                if (cont) tryNextProc()
              }
          }
          deferTx {
            actionOptCancel.enabled = true
          }
          tx.afterCommit(processor.start())
        }

        private def pushProcs(xs: List[Proc[T]])(implicit tx: T): Unit = {
          val procList = xs.map(p => tx.newHandle[Proc[T]](p))
          optParStackRef.transform(xs => xs ::: procList)
          if (optParProcessorRef().isEmpty) {
            tryNextProc()
          }
        }

        private def tryNextProc()(implicit tx: T): Unit = {
          val procOpt = optParStackRef.transformAndExtract {
            case x :: xs  => (xs, Some(x()))
            case Nil      => (Nil, None)
          }
          procOpt.fold[Unit] {
            tx.afterCommit {
              println("Done optimize/parametrize.")
            }
            deferTx {
              actionOptCancel.enabled = false
            }
          } { p =>
            runProc(p)
          }
        }

        override def canImport(support: TransferSupport): Boolean = enabled && {
          val t = support.getTransferable
          // t.getTransferDataFlavors.foreach(println)
          t.isDataFlavorSupported(ObjView.Flavor) || t.isDataFlavorSupported(FolderView.SelectionFlavor)
        }

        override def importData(support: TransferSupport): Boolean = enabled && {
          val t = support.getTransferable
          if (t.isDataFlavorSupported(ObjView.Flavor)) {
            val td = DragAndDrop.getTransferData(t, ObjView.Flavor)
            td.universe.workspace == universe.workspace && ({ val tpe = td.view.factory.tpe; tpe == Proc || tpe == Folder } && {
              universe.cursor.step { implicit tx =>
                td.view.asInstanceOf[ObjView[T]].obj match {
                  case p: Proc[T] =>
                    pushProcs(p :: Nil)
                    true

                  case f: Folder[T] =>
                    val pList = f.iterator.collect {
                      case p: Proc[T] => p
                    } .toList
                    pList.nonEmpty && {
                      pushProcs(pList)
                      true
                    }

                  case _ =>
                    assert(false, td.view.factory.tpe)
                    false
                }
              }
            })
          } else {
            val td = DragAndDrop.getTransferData(t, FolderView.SelectionFlavor)
            td.universe.workspace == universe.workspace && { val tpeSet = td.types; tpeSet.contains(Proc.typeId) } && {
              universe.cursor.step { implicit tx =>
                val pList = td.selection.asInstanceOf[FolderView.Selection[T]].flatMap { nv =>
                  nv.modelData() match {
                    case p: Proc[T] => Some(p)
                    case _ => None
                  }
                }
                pList.nonEmpty && {
                  pushProcs(pList)
                  true
                }
              }
            }
          }
        }
      }

      peer.setTransferHandler(TH)
      tooltip = s"Drop Procs or Folder to $nameOptPar"
    }

    private def mkUGenSet(s: String): Set[String] =
      s.split(",").iterator.map(_.trim).toSet[String]

    def init(n: Negatum[T])(implicit tx: T): this.type = {
      val attr  = n.attr
      implicit val intEx    : IntObj    .type = IntObj
      implicit val doubleEx : DoubleObj .type = DoubleObj
//      implicit val booleanEx: BooleanObj.type = BooleanObj

      class Field(name: String, view: View[T]) {
        lazy val label : Label      = new Label(s"$name:")
        def      editor: Component  = view.component
      }

      def mkIntField(name: String, key: String, default: Int): Field = {
        val view = IntSpinnerView.optional[T](CellView.attr[T, Int, IntObj](attr, key),
          name = name, default = Some(default))
        new Field(name, view)
      }

      def mkDoubleField(name: String, key: String, default: Double): Field = {
        val view = DoubleSpinnerView.optional[T](CellView.attr[T, Double, DoubleObj](attr, key),
          name = name, default = Some(default))
        new Field(name, view)
      }

      def mkBooleanField(name: String, key: String, default: Boolean): Field = {
        val view = BooleanCheckBoxView.optional[T](CellView.attr[T, Boolean, BooleanObj](attr, key),
          name = name, default = default)
        new Field(name, view) {
          override lazy val editor: Component = {
            val res = view.component
            res.text = null
            res
          }
        }
      }

      def mkStringField(name: String, key: String, default: String)(validate: T => String => Unit): Field = {
        val cell = CellView.attr[T, String, StringObj](attr, key) // .getOrElse(CellView.const(""))
        cell.react { implicit tx => vOpt => vOpt.foreach(v => validate(tx)(v)) }
        val view = StringFieldView.optional[T](cell, name = name)
        new Field(name, view)
      }

      // def mkEmptyField(): Field = new Field("", View.wrap[S](Swing.HGlue))

      import Negatum.Config.default._
      import Negatum._

      case class Grid(title: String, fields: Seq[Field])

      val fSeed = {
        val name = "Seed"
        val view = IntSpinnerView.optional[T](CellView.attr[T, Int, IntObj](attr, attrSeed),
          name = name, default = None)
        new Field(name, view)
      }

      val fGenPopulation      = mkIntField    ("Population"             , attrGenPopulation , gen.population)
      val fGenConstProb       = mkDoubleField ("Prob. of Constants"     , attrGenProbConst  , gen.probConst)
      val fGenMinVertices     = mkIntField    ("Min. # of Vertices"     , attrGenMinVertices, gen.minVertices)
      val fGenMaxVertices     = mkIntField    ("Max. # of Vertices"     , attrGenMaxVertices, gen.maxVertices)
      val fGenDefaultProb     = mkDoubleField ("Prob. of Default Values", attrGenProbDefault, gen.probDefault)
      lazy val setFullNames = UGens.seq.iterator.map(_.name).toSet
      val fGenAllowedUGens    = mkStringField ("Allowed UGens" , attrGenAllowedUGens, "") { implicit tx => v =>
        val setAllowed  = mkUGenSet(v)
        val setInvalid  = setAllowed.diff(setFullNames)
        if (setInvalid.nonEmpty) tx.afterCommit {
          println("WARNING: The following UGens are unknown and will be ignored: ")
          println(setInvalid.mkString(","))
        }
      }
      val gridGen = Grid("Generation",
        Seq(fGenPopulation, fGenConstProb, fGenMinVertices, fGenDefaultProb, fGenMaxVertices, fSeed, fGenAllowedUGens))

      val fEvalMinFreq        = mkIntField    ("Min Freq [Hz]"          , attrEvalMinFreq   , eval.minFreq)
      val fEvalMaxFreq        = mkIntField    ("Max Freq [Hz]"          , attrEvalMaxFreq   , eval.maxFreq)
      val fEvalNumMFCC        = mkIntField    ("# of MFCC"              , attrEvalNumMFCC   , eval.numMFCC)
      val fEvalNumMel         = mkIntField    ("# of Mel Bands"         , attrEvalNumMel    , eval.numMel )
      val fEvalMaxBoost       = mkDoubleField ("Max. Boost"             , attrEvalMaxBoost  , eval.maxBoost)
      val fEvalTempWeight     = mkDoubleField ("Temporal Weight"        , attrEvalTimeWeight, eval.timeWeight)
      val gridEval = Grid("Evaluation",
        Seq(fEvalMinFreq, fEvalNumMFCC, fEvalMaxFreq, fEvalNumMel, fEvalMaxBoost, fEvalTempWeight))

      val fBreedSelFraction   = mkDoubleField ("Selection Fraction"     , attrBreedSelectFraction, breed.selectFraction)
      val fBreedElitism       = mkIntField    ("Elitism"                , attrBreedElitism  , breed.elitism)
      val fBreedMinMut        = mkIntField    ("Min. # of Mutations"    , attrBreedMinMut   , breed.minMut)
      val fBreedMaxMut        = mkIntField    ("Max. # of Mutations"    , attrBreedMaxMut   , breed.maxMut)
      val fBreedProbMut       = mkDoubleField ("Prob. of Mutation"      , attrBreedProbMut  , breed.probMut)
      val fBreedGolem         = mkIntField    ("# of Golems"            , attrBreedGolem    , breed.golem)
      val gridBreed = Grid("Breeding",
        Seq(fBreedSelFraction, fBreedProbMut, fBreedElitism, fBreedMinMut, fBreedGolem, fBreedMaxMut))

      val fOptParDur          = mkDoubleField ("Analysis Duration [s]"  , attrOptDuration     , defaultOptAnaDur(n))
      val fOptExpandProtect   = mkBooleanField("Expand Protect Elements", attrOptExpandProtect, default = true)
      val fOptExpandIO        = mkBooleanField("Expand I/O Elements"    , attrOptExpandIO     , default = true)
      val fOptMono            = mkBooleanField("Force Monophonic"       , attrOptMono         , default = true)
      val fParMaxParam        = mkIntField    ("Max. # of Parameters"   , attrParMaxParams    , default = 5)
      val fParNumChannels     = mkIntField    ("Parameter Channels"     , attrParNumChannels  , default = 2)
      val gridOptPar = Grid("",
        Seq(fOptParDur, fOptExpandProtect, fOptExpandIO, fOptMono, fParMaxParam, fParNumChannels))

      deferTx {
        def mkGrid(grids: Seq[Grid], columns: Int = 2): Component = {
          val gridV = grids.zipWithIndex.flatMap { case (grid, gi) =>
            val gridSz  = grid.fields.size
            val gridC0  = grid.fields.flatMap(v => v.label :: v.editor :: Nil)
            val gridC1  = if (gridSz % columns == 0) gridC0 else gridC0 ++ (Swing.HGlue :: Swing.HGlue :: Nil)
            val gridC   = if (grid.title.isEmpty) gridC1 else {
              val lbTitle         = new Label(s"<HTML><BODY><B>${grid.title}</B></BODY>", Swing.EmptyIcon, Alignment.Left)
              lbTitle.border  = Swing.EmptyBorder(if (gi == 0) 0 else 8, 0, 4, 0)
              val rowTitle        = lbTitle :: Swing.HGlue :: Swing.HGlue :: Swing.HGlue :: Nil
              rowTitle ++ gridC1
            }
            val _gridV   = gridC.grouped(columns * 2).toSeq
            _gridV
          }
          val gridH   = gridV.transpose
          new GroupPanel {
            horizontal = Seq(gridH.map(col => Par          (col.map(c => GroupPanel.Element(c)): _*)): _*)
            vertical   = Seq(gridV.map(row => Par(Baseline)(row.map(c => GroupPanel.Element(c)): _*)): _*)
            border     = {
              val empty = Swing.EmptyBorder(4)
//              if (title.isEmpty) empty else Swing.TitledBorder(empty, s"<HTML><BODY><B>$title</B></BODY>")
              empty
            }
          }
        }
        val panelParams = mkGrid(Seq(gridGen, gridEval, gridBreed))
        panelParams.border = Swing.EmptyBorder(4, 0, 4, 0)

        val panelOpt = mkGrid(gridOptPar :: Nil, columns = 1)

        guiInit(panelParams, panelOpt)
      }
      this
    }

    override def negatum(implicit tx: T): Negatum[T] = negatumH()

//    def rendering(implicit tx: T): Option[Rendering[T, Unit]]  = renderRef()

    private lazy val actionOptCancel: swing.Action = new swing.Action(null) {
      def apply(): Unit = {
        cursor.step { implicit tx =>
          cancelOpt()
        }
        enabled = false
      }

      enabled = false
    }

    private def guiInit(panelParams: Component, panelOpt: Component): Unit = {
      val ggProgress: ProgressBar = new ProgressBar
      ggProgress.max = 160

      val actionCancel: swing.Action = new swing.Action(null) {
        def apply(): Unit = cursor.step { implicit tx =>
          renderRef.swap(None).foreach(_.cancel())
        }
        enabled = false
      }

      val actionStop: swing.Action = new swing.Action(null) {
        def apply(): Unit = cursor.step { implicit tx =>
          renderRef.swap(None).foreach(_.stop())
        }
        enabled = false
      }

      val ggCancel  = GUI.toolButton(actionCancel, raphael.Shapes.Cross        , tooltip = "Abort Rendering")
      val ggStop    = GUI.toolButton(actionStop  , raphael.Shapes.TransportStop, tooltip = "Stop Rendering and Update Table")

      val mNumIterations  = new SpinnerNumberModel(1, 1, 65536, 1)
      val ggNumIterations = new Spinner(mNumIterations)

      val actionRender = new swing.Action("Evolve") { self =>
        def apply(): Unit = {
          val numIterations = mNumIterations.getNumber.intValue()
          val ok = cursor.step { implicit tx =>
            renderRef().isEmpty && {
              val obj   = negatumH()
              val attr  = obj.attr
              import Negatum.Config.default._
              import Negatum._
              val seed      = attr.$[IntObj](attrSeed).map(_.value.toLong).getOrElse(System.currentTimeMillis())

              val cGen      = Negatum.Generation(
                population    = attr.$[IntObj    ](attrGenPopulation  ).map(_.value).getOrElse(gen.population),
                probConst     = attr.$[DoubleObj ](attrGenProbConst   ).map(_.value).getOrElse(gen.probConst),
                minVertices   = attr.$[IntObj    ](attrGenMinVertices ).map(_.value).getOrElse(gen.minVertices),
                maxVertices   = attr.$[IntObj    ](attrGenMaxVertices ).map(_.value).getOrElse(gen.maxVertices),
                probDefault   = attr.$[DoubleObj ](attrGenProbDefault ).map(_.value).getOrElse(gen.probDefault),
                allowedUGens  = attr.$[StringObj ](attrGenAllowedUGens).map { sObj =>
                  mkUGenSet(sObj.value)
                } .getOrElse(Set.empty[String]),
              )
              val cBreed    = Negatum.Breeding(
                selectFraction  = attr.$[DoubleObj ](attrBreedSelectFraction).map(_.value).getOrElse(breed.selectFraction),
                elitism     = attr.$[IntObj    ](attrBreedElitism   ).map(_.value).getOrElse(breed.elitism),
                minMut      = attr.$[IntObj    ](attrBreedMinMut    ).map(_.value).getOrElse(breed.minMut),
                maxMut      = attr.$[IntObj    ](attrBreedMaxMut    ).map(_.value).getOrElse(breed.maxMut),
                probMut     = attr.$[DoubleObj ](attrBreedProbMut   ).map(_.value).getOrElse(breed.probMut),
                golem       = attr.$[IntObj    ](attrBreedGolem     ).map(_.value).getOrElse(breed.golem)
              )
              val cEval     = Negatum.Evaluation(
                minFreq     = attr.$[IntObj    ](attrEvalMinFreq    ).map(_.value).getOrElse(eval.minFreq ),
                maxFreq     = attr.$[IntObj    ](attrEvalMaxFreq    ).map(_.value).getOrElse(eval.maxFreq ),
                numMFCC     = attr.$[IntObj    ](attrEvalNumMFCC    ).map(_.value).getOrElse(eval.numMFCC ),
                numMel      = attr.$[IntObj    ](attrEvalNumMel     ).map(_.value).getOrElse(eval.numMel  ),
                maxBoost    = attr.$[DoubleObj ](attrEvalMaxBoost   ).map(_.value).getOrElse(eval.maxBoost),
                timeWeight  = attr.$[DoubleObj ](attrEvalTimeWeight ).map(_.value).getOrElse(eval.timeWeight)
              )
              val cPenalty  = Negatum.Penalty()
              val config    = Negatum.Config(seed = seed, generation = cGen, breeding = cBreed, evaluation = cEval,
                penalty = cPenalty)

              def finished()(implicit tx: T): Unit = {
                renderRef() = None
                deferTx {
                  actionCancel.enabled  = false
                  actionStop  .enabled  = false
                  self.enabled          = true
                }
              }

              val rendering = obj.run(config, iterations = numIterations)
              /* val obs = */ rendering.reactNow { implicit tx => {
                case Rendering.Completed(Success(_)) => finished()
                case Rendering.Completed(Failure(Rendering.Cancelled())) => finished()
                case Rendering.Completed(Failure(ex)) =>
                  finished()
                  deferTx(ex.printStackTrace())
                case Rendering.Progress(amt) =>
                  deferTx {
                    ggProgress.value = (amt * ggProgress.max).toInt
                  }
              }}
              renderRef() = Some(rendering)
              true
            }
          }
          if (ok) {
            actionCancel.enabled = true
            actionStop  .enabled = true
            self        .enabled = false
          }
        }
      }
      val ggRender  = GUI.toolButton(actionRender, raphael.Shapes.Biohazard)

      val actionAnalyze = new swing.Action("Analyze...") {
        self =>
        def apply(): Unit = cursor.step { implicit tx =>
          FeatureAnalysisFrame(negatum)
        }
      }
      val ggAnalyze = GUI.toolButton(actionAnalyze, raphael.Shapes.View)

      //            val ggDebug = Button("Debug") {
      //              renderRef.single.get.foreach { r =>
      //                val ctrl = r.control
      //                println(ctrl.stats)
      //                ctrl.debugDotGraph()
      //              }
      //            }

      val ggDropProc = new DropProcLabel
      val ggOptPar = Button("…") {
        Dialog.showMessage(
          parent      = null,
          title       = s"$nameOptPar Settings",
          messageType = Dialog.Message.Plain,
          message     = panelOpt.peer
        )
      }
      ggOptPar.tooltip = "Click to configure"
//      ggOptPar.peer.putClientProperty("styleId", "icon-hover")

      val ggOptType = new ComboBox[String](mOptType)

      val ggOptCancel = GUI.toolButton(actionOptCancel, raphael.Shapes.Cross, tooltip = s"Abort $nameOptPar")

      val panelControl = new FlowPanel(new Label("Iterations:"),
        ggNumIterations, ggProgress, ggCancel, ggStop, ggRender, ggAnalyze, ggOptType, ggOptPar, ggOptCancel, ggDropProc)
      component = new BorderPanel {
        add(panelParams , BorderPanel.Position.Center)
        add(panelControl, BorderPanel.Position.South )
      }
    }

    private def cancelOpt()(implicit tx: T): Unit = {
      optParStackRef() = Nil
      optParProcessorRef.swap(None).foreach(_.abort())
    }

    def dispose()(implicit tx: T): Unit = {
      renderRef.swap(None).foreach(_.cancel ())
      cancelOpt()
    }
  }
}