/*
 *  FeatureAnalysisFrame.scala
 *  (Negatum)
 *
 *  Copyright (c) 2016-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.negatum
package gui

import de.sciss.lucre.{Txn, synth}
import de.sciss.mellite.{UniverseHandler, WorkspaceWindow}
import de.sciss.negatum.gui.impl.FeatureAnalysisFrameImpl

object FeatureAnalysisFrame extends WorkspaceWindow.Key {
  def apply[T <: synth.Txn[T]](negatum: Negatum[T])(implicit tx: T,
                                                    handler: UniverseHandler[T]): FeatureAnalysisFrame[T] =
    FeatureAnalysisFrameImpl[T](negatum)

  type Repr[T <: Txn[T]] = FeatureAnalysisFrame[T]
}
trait FeatureAnalysisFrame[T <: Txn[T]] extends WorkspaceWindow[T] {
  type Repr[~ <: Txn[~]] = FeatureAnalysisFrame[~]

  override def view: FeatureAnalysisView[T]
}