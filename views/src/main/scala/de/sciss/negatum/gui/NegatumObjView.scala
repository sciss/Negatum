/*
 *  NegatumObjView.scala
 *  (Negatum)
 *
 *  Copyright (c) 2016-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.negatum.gui

import de.sciss.desktop
import de.sciss.desktop.OptionPane
import de.sciss.icons.raphael
import de.sciss.lucre.expr.CellView
import de.sciss.lucre.swing.Window
import de.sciss.lucre.synth.Txn
import de.sciss.lucre.{Obj, Source, Txn => LTxn}
import de.sciss.mellite.impl.WorkspaceWindowImpl
import de.sciss.mellite.impl.objview.ObjListViewImpl.{EmptyRenderer, NonEditable}
import de.sciss.mellite.impl.objview.ObjViewImpl
import de.sciss.mellite.{AudioCueObjView, ObjListView, ObjView, UniverseHandler, UniverseObjView, WorkspaceWindow}
import de.sciss.negatum.Negatum
import de.sciss.proc.{AudioCue, Universe}
import de.sciss.processor.Processor.Aborted

import javax.swing.Icon
import scala.concurrent.Future

object NegatumObjView extends ObjListView.Factory {
  type E[~ <: LTxn[~]] = Negatum[~]
  val icon          : Icon      = ObjViewImpl.raphaelIcon(raphael.Shapes.Biohazard)
  val prefix        : String    = "Negatum"
  def humanName     : String    = prefix
  def tpe           : Obj.Type  = Negatum
  def category      : String    = ObjView.categComposition
  def hasMakeDialog : Boolean   = true

  private[this] lazy val _init: Unit = ObjListView.addFactory(this)

  def init(): Unit = {
    _init
//    SVMModelObjView.init()
//    SOMObjView     .init()
  }

  def mkListView[T <: Txn[T]](obj: Negatum[T])(implicit tx: T): NegatumObjView[T] with ObjListView[T] =
    new Impl(tx.newHandle(obj)).initAttrs(obj)

  final case class Config[T <: LTxn[T]](name: String, audioCue: AudioCueObjView.SingleConfig[T])

  def canMakeObj: Boolean = true

  override def initMakeCmdLine[T <: Txn[T]](args: List[String])(implicit universe: Universe[T]): MakeResult[T] =
    Future.failed(new NotImplementedError("Make Negatum from command line"))

  override def initMakeDialog[T <: Txn[T]](window: Option[desktop.Window])
                                          (implicit universe: Universe[T]): MakeResult[T] = {
    import de.sciss.lucre.synth.Executor.executionContext
    val opt = OptionPane.textInput(message = s"Enter initial ${prefix.toLowerCase} name:",
      messageType = OptionPane.Message.Question, initial = prefix)
    opt.title = s"New $prefix"
    val res = opt.show(window)
    res match {
      case Some(name) =>
        AudioCueObjView.initMakeDialog[T](window).map { cueConfig =>
          cueConfig.headOption match {
            case Some(audioCue) =>
              val config = Config(name = name, audioCue = audioCue)
              config

            case None =>
              throw Aborted()
          }
        }

      case None => Future.failed(Aborted())
    }
  }

  def makeObj[T <: Txn[T]](config: Config[T])(implicit tx: T): List[Obj[T]] = {
    val res1 = AudioCueObjView.makeObj(config.audioCue :: Nil)
    val templateOpt = res1.collectFirst {
      case a: AudioCue.Obj[T] => a
    }
    templateOpt.fold(res1) { template =>
      val obj  = Negatum[T](template)
      import de.sciss.proc.Implicits._
      if (config.name.nonEmpty) obj.name = config.name
      obj :: obj.population :: res1 // expose population until we have a proper editor
    }
  }

  private object NegatumFrame extends WorkspaceWindow.Key {
    type Repr[T <: LTxn[T]] = NegatumFrame[T]
  }
  private trait NegatumFrame[T <: LTxn[T]] extends WorkspaceWindow[T] {
    type Repr[~ <: LTxn[~]] = NegatumFrame[~]
  }

  final class Impl[T <: Txn[T]](val objH: Source[T, Negatum[T]])
    extends NegatumObjView[T]
      with ObjListView[T]
      with ObjViewImpl.Impl[T]
      with EmptyRenderer[T]
      with NonEditable[T] {

    override def obj(implicit tx: T): Negatum[T] = objH()

    type E[~ <: LTxn[~]] = Negatum[~]

    def factory: ObjView.Factory = NegatumObjView

    override def isViewable = true

    override def openView(parent: Option[Window[T]])(implicit tx: T, _handler: UniverseHandler[T]): Option[Window[T]] = {
      val _obj  = objH()
      val res   = _handler(_obj, NegatumFrame) {
        val title     = CellView.name(_obj)
        val _view     = NegatumView(_obj)
        val fr = new NegatumFrame[T] with WorkspaceWindowImpl[T] {

          // XXX TODO
          override def supportsNewWindow: Boolean = false

          override def newWindow()(implicit tx: T): NegatumFrame[T] = throw new UnsupportedOperationException

          override def key: Key = NegatumFrame

          override val handler: UniverseHandler[T] = _handler

          override val view: UniverseObjView[T] = _view
        }
        fr.init().setTitle(title)
      }
      Some(res)
    }
  }
}
trait NegatumObjView[T <: LTxn[T]] extends ObjView[T] {
  type Repr = Negatum[T]
}