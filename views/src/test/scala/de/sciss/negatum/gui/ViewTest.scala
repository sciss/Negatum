package de.sciss.negatum
package gui

import de.sciss.audiofile.AudioFile
import de.sciss.desktop.Menu
import de.sciss.desktop.impl.SwingApplicationImpl
import de.sciss.lucre.synth.InMemory
import de.sciss.lucre.{Artifact, ArtifactLocation, DoubleObj, LongObj}
import de.sciss.mellite.{Application, UniverseHandler}
import de.sciss.proc.{AudioCue, AuralSystem, Code, Universe}

import java.io.File
import scala.collection.immutable.{Seq => ISeq}

object ViewTest extends SwingApplicationImpl[Application.Document]("Test") with Application {
  type S = InMemory
  type T = InMemory.Txn

//  println(UGens.seq.filter(_.name.contains("Noise")))

  override protected def init(): Unit = {
    require (args.length == 1, "Must give template sound file as argument")
    val path  = args(0)
    val f     = new java.io.File(path)
    val spec  = AudioFile.readSpec(f)

    Application.init(this)
    Negatum.init()

    implicit val system: S = InMemory()
    system.step { implicit tx =>
      val loc   = ArtifactLocation.newConst[T](f.getParentFile.toURI)
      val art   = Artifact[T](loc, Artifact.Child(f.getName))
      val cue   = AudioCue.Obj[T](art, spec, LongObj.newConst(0L), DoubleObj.newConst(1.0))
      val n     = Negatum[T](cue)
      val view  = NegatumObjView.mkListView(n)
      implicit val u : Universe[T]        = Universe.dummy[T]
      implicit val uh: UniverseHandler[T] = UniverseHandler[T]()
      view.openView(None)
    }
  }

  // --- minimum impl ---

  override protected lazy val menuFactory: Menu.Root = {
    Menu.Root()
      .add(Menu.Group("file", "File").add(Menu.Item("close", "Close")))
      .add(Menu.Group("edit", "Edit")
        .add(Menu.Item("undo", "Undo"))
        .add(Menu.Item("redo", "Redo"))
      )
  }

  // --- dummy impl ---

  override def topLevelObjects: ISeq[String] = Nil

  override def objectFilter: String => Boolean = _ => true

  override implicit def auralSystem: AuralSystem = throw new NotImplementedError()

  override implicit def compiler: Code.Compiler = throw new NotImplementedError()

  override def cacheDir: File = new java.io.File(sys.props("java.io.tmpdir"))

}