/*
 *  Parametrize.scala
 *  (Negatum)
 *
 *  Copyright (c) 2016-2022 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.negatum

import de.sciss.audiofile.AudioFile
import de.sciss.file._
import de.sciss.fscape.Graph
import de.sciss.log.Level
import de.sciss.lucre.synth.{Executor, InMemory}
import de.sciss.lucre.{Artifact, ArtifactLocation, DoubleObj, IntObj}
import de.sciss.mellite.{Application, Prefs}
import de.sciss.negatum.Negatum.{SynthGraphT, log}
import de.sciss.negatum.impl.{Chromosome, MkSynthGraph, MkTopology, ParamRanges}
import de.sciss.numbers
import de.sciss.proc.{Bounce, FScape, ParamSpec, Proc, Runner, TimeRef, Universe, Warp}
import de.sciss.processor.impl.ProcessorImpl
import de.sciss.processor.{Processor, ProcessorFactory}
import de.sciss.span.Span
import de.sciss.synth.proc.graph.{Attribute, Param}
import de.sciss.synth.ugen.RandID
import de.sciss.synth.{GE, SynthGraph, UGenSpec, audio}

import java.awt.EventQueue
import java.util.concurrent.TimeUnit
import scala.collection.immutable.{IndexedSeq => Vec}
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future, Promise}
import scala.swing.Swing
import scala.util.control.NonFatal

object Parametrize extends ProcessorFactory {
  type Repr     = Generic
  type Product  = Result

  /** @param graph  the input graph modified by replacing constants with controls
    * @param specs  the corresponding parameters as tuples (default, spec), sorted
    *               from highest to lowest priority (perceived impact on the sonic result)
    */
  case class Result(graph: SynthGraph, specs: Seq[(Double, ParamSpec)] /*, source: String*/)

  /** Parametrizes a graph by identifying constants whose mutation yields
    * significant sonic differences.
    *
    * @param graph        the graph to parametrize
    * @param sampleRate   the sample rate to use during internal bouncing
    * @param analysisDur  the duration of the signal analysis in seconds. The process only
    *                     looks at this interval to determine which constants are relevant
    * @param maxParams    maximum number of parameters to generate
    */
  final case class Config(graph         : SynthGraph,
                          sampleRate    : Double,
                          analysisDur   : Double  = 2.0,
                          maxParams     : Int     = 5,
                          expandProtect : Boolean = true,
                          expandIO      : Boolean = true,
                          mono          : Boolean = false,
                         ) {
    require(analysisDur > 0.0)

    override def toString = s"$productPrefix(SynthGraph@${graph.hashCode().toHexString}, $sampleRate, $analysisDur)"
  }

  protected def prepare(config: Config): Prepared = {
    new Impl(config)
  }

  private final class Impl(config: Config) extends ProcessorImpl[Product, Processor[Product]]
    with Processor[Product] {

    private[this] val inMemory = InMemory()

    // must run on EDT because of preferences
    private def bounceVariants(graph: SynthGraph, values: Vec[Double], audioF: File, duration: Double, sampleRate: Int,
                               valueKey: String = "value")/*(implicit timer: Timer)*/: Future[Any] = {
      type IS = InMemory
      type I  = InMemory.Txn
      implicit val iCursor: IS = inMemory

      // val exp = ExprImplicits[I]

      val (objH, _u) = inMemory.step { implicit tx =>
        values.zipWithIndex.map { case (value, gi) =>
          val proc = Proc[I]()
          val graphP = graph.copy(
            sources = RandID.ir(gi) +: graph.sources
          )
          proc.graph() = graphP
          proc.attr.put("out", IntObj.newConst(gi))
          proc.attr.put(valueKey, DoubleObj.newConst(value))
          tx.newHandle(proc)
        } -> Universe.dummy[I]
      }
      implicit val u: Universe[I] = _u

      val bncCfg = Bounce.Config[I]()
      bncCfg.group = objH // :: Nil
      Application.applyAudioPreferences(bncCfg.server, bncCfg.client, useDevice = false, pickPort = false)
      val sCfg = bncCfg.server
      sCfg.nrtOutputPath = audioF.path
      sCfg.inputBusChannels = 0
      sCfg.outputBusChannels = values.size
      val numPrivate = Prefs.audioNumPrivate.getOrElse(Prefs.defaultAudioNumPrivate)
      import numbers.Implicits._
      sCfg.audioBusChannels = (sCfg.outputBusChannels + numPrivate).nextPowerOfTwo
      sCfg.wireBuffers = math.max(sCfg.wireBuffers, 1024) // possibly higher than default
      //    sCfg.blockSize          = 64   // configurable through Mellite preferences now
      sCfg.sampleRate = sampleRate
      // bc.init : (S#Tx, Server) => Unit
      bncCfg.span = Span(0L, (duration * TimeRef.SampleRate).toLong)
      val bnc0 = Bounce[I]().apply(bncCfg)
      bnc0.start()
      // bnc0.startWithTimeout(10.0)
      Executor.timeOut(bnc0, 10L, TimeUnit.SECONDS)
    }

    private def runOne(topIn: SynthGraphT, use: Use, dur: Double,
                       sampleRate: Double)/*(implicit timer: Timer)*/: Future[Option[RunOne]] = {
      val Use(vc, min, max) = use // constWithUse(0) // .head
      val testValues  = mkTestValues(min = min, max = max)
      val default     = vc.f.toDouble
      val values      = default +: testValues
      val vCtl        = ControlVertex("value", Vector(vc.f))
      val topTest = {
        val t0 = topIn.addVertex(vCtl)
        Chromosome.replaceVertex(t0, vOld = vc, vNew = vCtl)
      }
      val graph = MkSynthGraph(topTest)

      //    val bncF = file("/data/temp/_killme.aif")
      val bncF  = File.createTemp(suffix = ".aif")
      val corrF = File.createTemp(suffix = ".aif")

      val futBnc = bounceVariants(
        graph       = graph,
        values      = values,
        audioF      = bncF,
        duration    = dur, // tempSpec.numFrames/tempSpec.sampleRate
        sampleRate  = sampleRate.toInt,
      )
      //    currentBnc = Some(futBnc)

      log.debug("Making test bounce...")

      val futCorr0 = futBnc.flatMap { _ =>
        log.debug("Correlating...")

        type I = InMemory.Txn
        type IS = InMemory
        implicit val iCursor: IS = inMemory

        val r = inMemory.step { implicit tx =>
          val f = FScape[I]()
          f.graph() = gCorr
          val bncLoc  = ArtifactLocation.newConst[I](bncF .parent.toURI)
          val corrLoc = ArtifactLocation.newConst[I](corrF.parent.toURI)
          f.attr.put("in" , Artifact[I](bncLoc  , Artifact.Child(bncF.name)))
          f.attr.put("out", Artifact[I](corrLoc , Artifact.Child(corrF.name)))
          implicit val u: Universe[I] = Universe.dummy[I]
          //        implicit val tgt: ITargets[I] = ITargets.apply
          //        import u.workspace
          //        implicit val undo: UndoManager[I] = UndoManager.dummy[I]
          //        implicit val ctx: Context[I] = Context[I]()
          //        val _runMap = ISeq.tabulate(values.size) { ch =>
          //          val vr = Var(-1.0)
          //          val pair: Ex[(String, Double)] = (s"out-$ch", vr)
          //          val _pairI = pair.expand[I]
          //          _pairI
          //        }
          val _r = f.run(attr =
            Runner.emptyAttr // new IExprAsRunnerMap[I](_runMap, tx)
          )

          _r
        }

//        val ttTimeOut = new TimerTask {
//          def run(): Unit = {
//            log.warn("FScape timeout")
//            inMemory.step { implicit tx => r.cancel() }
//          }
//        }

        val futFSc = r.control.status
        Executor.timeOut(futFSc, 15L, TimeUnit.SECONDS)
      }

      val futCorr = futCorr0.transform { tr =>
        bncF.delete()
        tr
      }

      val futOut0 = futCorr.map { _ =>
        val afCorr = AudioFile.openRead(corrF)
        try {
          val b = afCorr.buffer(2)
          afCorr.read(b)
          val bt = b.transpose
          val dCorr = bt(0). /*iterator.map(_.toDouble).*/ toVector.tail
          val dEn   = bt(1). /*iterator.map(_.toDouble).*/ toVector.tail

          val idxStart  = dEn.indexWhere(_ > 0.01) // ca. -40 dB
          val idxStop   = dEn.lastIndexWhere(_ > 0.01) + 1
          if (idxStart >= 0 && idxStop > idxStart) {
            val minCorr = dCorr.slice(idxStart, idxStop).min
            if (minCorr < 0.7) {
              val valueRange  = testValues.slice(idxStart, idxStop)
              val valueGP     = use.vc.f.toDouble
              val valueMin    = math.min(valueGP, valueRange.head)
              val valueMax    = math.max(valueGP, valueRange.last)
              if (valueMin == valueMax) {
                log.warn("valueMin == valueMax? range is " + valueRange + "; gp is " + valueGP.toString)
              } else if (valueMin.isNaN || valueMax.isNaN) {
                log.warn("min " + valueMin.toString + "; max " + valueMax.toString)
              }
              Some(RunOne(min = valueMin, max = valueMax, corr = minCorr))

            } else {
              None
            }
          } else {
            None
          }

        } finally {
          afCorr.cleanUp()
        }
      }

      val futOut = futOut0.transform { tr =>
        corrF.delete()
        tr
      }

      futOut

    }

    override protected def body(): Result = {
      val pBnc = runGraph()
//      await(pBnc, target = 0.5)
      val res = Await.result(pBnc, Duration.Inf)
      progress = 1.0
      res
    }

    private def runGraph() /*(implicit timer: Timer)*/: Future[Result] = {
      import config.{graph => _gIn, _}

      val t0 = System.currentTimeMillis()

      val topIn     = MkTopology(_gIn)
      val numConst  = topIn.vertices.count(_.isConstant)
      val constants = topIn.vertices.collect {
        case vc: Vertex.Constant => vc
      }
      log.debug(s"Num.constants $numConst")

      val constWithUse: Vec[Use] = constants.map { vc =>
        val vNameOut: List[(String, String)] = Chromosome.getArgUsages(topIn, vc).flatMap { edge =>
          edge.sourceVertex match {
            case vu: Vertex.UGen =>
              Some((vu.info.name, edge.inlet))

            case _ => None
          }
        }
        val rOutSeq = vNameOut.map { case (uName, pName) =>
          val r = ParamRanges.map.get(uName)
          val inf = Double.PositiveInfinity
          val (min, max) = r.foldLeft((inf, -inf)) { case ((minA, maxA), info) =>
            val pOpt = info.params.get(pName)
            pOpt.fold((-inf, inf)) { p =>
              (p.lo.fold(-inf)(m => math.min(m.value, minA)), p.hi.fold(inf)(m => math.max(m.value, maxA)))
            }
          }
          (if (min == inf) -inf else min, if (max == -inf) inf else max)
        }
        // always "expand" the possible parameter range, as we'll anyway have a `Protect` in place
        val (minRed, maxRed) = rOutSeq.reduce[(Double, Double)] { case ((minA, maxA), (minB, maxB)) =>
          (math.min(minA, minB), math.max(maxA, maxB))
        }

        // we limit here (unless there is chosen boundary higher than +- 22k)
        val min = if (minRed.isInfinite) -sampleRate/2 else minRed
        val max = if (maxRed.isInfinite) +sampleRate/2 else maxRed

        Use(vc, min, max)
        //      topIn.edgeMap(vc)
      }
      //    val constWithUse = constWithUse0.filterNot { case (_, uses) =>
      //      uses.isEmpty || uses.forall(u => u._2.exists(_.dynamic))
      //    }

      log.debug(constants.map(_.f).toString)
      log.debug(constWithUse.mkString("\n"))

      /*

       take the current value. roundUpTo(0.1). if this is zero, add 0.1; go in up to
       16 octaves up (times 2, times 2, ...); or stop when reaching the worst upper
       param range; do the same in the opposite direction; if

       or more coarse; factor 3

       these 31 values:

       (0 until 15).map(i => 22050 / 2.5.pow(i)) ++ 0.0 ++ (0 until 15).map(i => -22050 / 2.5.pow(i))

       should suffice to detect if the parameter has sonic effect, and the approximate ranges

       if a param range is present, we could lower the factor 2.5 for more fine-grained boundaries

       we can bounce in "parallel" channels if we use multiple synths each of which has a unique RNG identifier,
       although perhaps this is not any faster than bouncing a "timeline" of 31 successive runs

       for the correlation purposes, we should insert the original value, so we have actually 32 runs

      */

      val futCorr = sequence(constWithUse) { use =>
        futEDT {
          runOne(topIn, use, dur = analysisDur, sampleRate = sampleRate)
        }
      }

      futCorr.map { seq =>
        val t1 = System.currentTimeMillis()
        log.info(s"  Done (took ${(t1 - t0) / 1000}s).")
        log.debug(seq.mkString("\n"))

        val hasRun: Seq[(RunOne, Use)] = (seq zip constWithUse).collect {
          case (Some(run), vc) => (run, vc)
        }
        val sortRun = hasRun.sortBy(_._1.corr).take(maxParams)

        if (log.level <= Level.Debug) {
          log.debug("\n--SEL---\n")
          log.debug(sortRun.mkString("\n"))
        } else {
          log.info(s"  num-params: ${sortRun.size}")
        }

        //          val topPatch = sortRun.zipWithIndex.foldLeft(topIn) { case (topAcc, ((run, use), idx)) =>
        //            import use.vc
        //            import numbers.Implicits._
        //            val v0      = vc.f.linLin(run.min, run.max, 0.0, 1.0)
        //            val vCtl    = ControlVertex(s"ctl_$idx", Vector(v0.toFloat))
        //            val nMul    = s"Bin_${BinaryOpUGen.Times.id}"
        //            val nAdd    = s"Bin_${BinaryOpUGen.Plus .id}"
        //            val sMul    = UGens.map(nMul)
        //            val sAdd    = UGens.map(nAdd)
        //            val vMul    = Vertex.UGen(sMul)
        //            val vAdd    = Vertex.UGen(sAdd)
        //            val vcMul   = Vertex.Constant((run.max - run.min).toFloat)
        //            val vcAdd   = Vertex.Constant( run.min.toFloat)
        //            var topOut  = topAcc
        //            topOut      = topOut.addVertex(vCtl)
        //            topOut      = topOut.addVertex(vMul)
        //            topOut      = topOut.addVertex(vAdd)
        //            topOut      = topOut.addEdge(Edge(vMul, vCtl  , "a")).get._1
        //            topOut      = topOut.addVertex(vcMul)
        //            topOut      = topOut.addEdge(Edge(vMul, vcMul , "b")).get._1
        //            topOut      = topOut.addEdge(Edge(vAdd, vMul  , "a")).get._1
        //            topOut      = topOut.addVertex(vcAdd)
        //            topOut      = topOut.addEdge(Edge(vAdd, vcAdd , "b")).get._1
        //            topOut      = Chromosome.replaceVertex(topOut, vOld = vc, vNew = vAdd)
        //            topOut
        //          }

        val topPatch = sortRun.zipWithIndex.foldLeft(topIn) { case (topAcc, ((_, use), idx)) =>
          import use.vc
          val vCtl    = ParamVertex(s"p${idx + 1}", /*spec,*/ Vector /*.fill(4)*/(vc.f))
          var topOut  = topAcc
          topOut      = topOut.addVertex(vCtl)
          topOut      = Chromosome.replaceVertex(topOut, vOld = vc, vNew = vCtl)
          topOut
        }

        val specs = sortRun.map { case (run, use) =>
          import use.vc
          val warp    = if (run.min.signum == run.max.signum) Warp.Exp else Warp.Lin
          val spec    = ParamSpec(run.min, run.max, warp)
          val default = spec.inverseMap(vc.f)
          (default, spec)
        }

        val gPatch = MkSynthGraph(topPatch,
          mono          = config.mono,
          expandProtect = config.expandProtect,
          expandIO      = config.expandIO
        )
        //      val srcPatch  = MkSynthGraphSource(gPatch)

        Result(gPatch, specs = specs /*, srcPatch*/)
      }
    }
  }

  private final case class ControlVertex(name: String, values: Vec[Float]) extends Vertex.UGen {
    val info: UGenSpec = UGenSpec(
      name        = "AudioControl",
      attr        = Set.empty,
      rates       = UGenSpec.Rates.Implied(audio, UGenSpec.RateMethod.Default),
      args        = Vector.empty,
      inputs      = Vector.empty,
      outputs     = Vector.tabulate(values.size) { _ =>
        UGenSpec.Output(name = None, shape = UGenSpec.SignalShape.Generic, variadic = None)
      },
      doc         = None,
      elemOption  = None,
    )

    def instantiate(ins: Vec[(AnyRef, Class[_])]): GE =
      Attribute.ar(name, values)

    def copy(): Vertex = ControlVertex(name = name, values = values)
  }

  private final case class ParamVertex(name: String, /*spec: ParamSpec,*/ default: Vec[Float]) extends Vertex.UGen {
    val info: UGenSpec = UGenSpec(
      name        = "ParamVertex",
      attr        = Set.empty,
      rates       = UGenSpec.Rates.Implied(audio, UGenSpec.RateMethod.Default),
      args        = Vector.empty,
      inputs      = Vector.empty,
      outputs     = Vector.tabulate(default.size) { _ =>
        UGenSpec.Output(name = None, shape = UGenSpec.SignalShape.Generic, variadic = None)
      },
      doc         = None,
      elemOption  = None,
    )

    def instantiate(ins: Vec[(AnyRef, Class[_])]): GE =
      Param.ar(name, default)

    def copy(): Vertex = ParamVertex(name = name, /*spec = spec,*/ default = default)
  }

  private def mkTestValues(min: Double, max: Double): Vec[Double] = {
    def genPos(n: Int, hi: Double): Vec[Double] = {
      val lo    = math.min(0.05, hi / 10000)
      val n1    = n - 1
      val f     = math.max(1.25, math.pow(hi / lo, 1.0 / n1))
      val vec0  = Vector.tabulate(n)(i => lo * math.pow(f, i))
      vec0.takeWhile(_ + 1.0e-4 < hi) :+ hi
    }

    def genNeg(n: Int, lo: Double): Vec[Double] = {
      val hi    = math.max(-0.05, lo / 10000)
      val n1    = n - 1
      val f     = math.max(1.25, math.pow(lo / hi, 1.0 / n1))
      val vec0  = Vector.tabulate(n)(i => hi * math.pow(f, n1 - i))
      lo +: vec0.dropWhile(_ - 1.0e-4 < lo)
    }

    def genBi(n: Int, lo: Double, hi: Double): Vec[Double] = {
      val n1    = n - 1
      val f     = math.max(1.25, math.pow(hi / lo, 1.0 / n1))
      val vec0  = if (lo > 0.0) Vector.tabulate(n)(i => lo * math.pow(f, i))
      else          Vector.tabulate(n)(i => hi * math.pow(f, n1 - i))
      lo +: vec0.dropWhile(_ - 1.0e-4 < lo).takeWhile(_ + 1.0e-4 < hi) :+ hi
    }

    if (min == 0.0) {
      assert (max > 0.0)
      0.0 +: genPos(n = 30, hi = max)

    } else if (max == 0.0) {
      assert (min < 0.0)
      genNeg(n = 30, lo = min) :+ 0.0

    } else if (min < 0.0 && max > 0.0) {  // different signum
      genNeg(n = 15, lo = min) ++ (0.0 +: genPos(n = 15, hi = max))

    } else {  // same signum
      assert (math.signum(min) == math.signum(max))
      genBi(n = 31, lo = min, hi = max)
    }
  }

  private final case class Use(vc: Vertex.Constant, min: Double, max: Double)

  //  final case class Corr(value: Double, amp: Double)
  //  final case class RunOne(testValues: Vec[Double], corr: Vec[Corr])
  private final case class RunOne(min: Double, max: Double, corr: Double)

  private def futEDT[A](body: => Future[A]): Future[A] =
    if (EventQueue.isDispatchThread) body else {
      val p = Promise[A]()
      Swing.onEDT {
        try {
          p.completeWith(body)
        } catch {
          case NonFatal(ex) =>
            p.tryFailure(ex)
        }
      }
      p.future
    }

  private def sequence[A, B](xs: Seq[A])(f: A => Future[B])(implicit exec: ExecutionContext): Future[Seq[B]] =
    xs.foldLeft(Future.successful(Vector.empty[B])) {
      case (acc, x) =>
        acc.flatMap(prev => f(x).map(prev :+ _))
    }

  lazy val gCorr: Graph = Graph {
    import de.sciss.fscape.Ops._
    import de.sciss.fscape.graph.{AudioFileIn => _, AudioFileOut => _, _}
    import de.sciss.fscape.lucre.graph._
    //val numFrames = 262144
    val inAll     = AudioFileIn("in")
    val numFrames = inAll.numFrames.toInt
    val inRef     = inAll.out(0)
    val inRefRvs  = ReverseWindow(inRef, numFrames)
    val convSize  = numFrames + numFrames - 1
    val fftSize   = convSize.nextPowerOfTwo // numFrames << 1
    //fftSize.poll("fftSize")
    val fftAll    = Real1FFT(inAll    , size = fftSize, mode = 1)
    val fftRef    = Real1FFT(inRefRvs , size = fftSize, mode = 1) * fftSize
    val eAll      = RunningSum(inAll.squared).last
    val eRef      = RunningSum(inRef.squared).last
    val prod      = fftAll.complex * fftRef
    val corr      = Real1IFFT(prod, fftSize, mode = 1)
    //Plot1D(corr, convSize min 1024, "corr")
    val corrMax0  = RunningMax(corr.abs).last
    val corrMax   = corrMax0 / (eAll + eRef) ++ (eAll / eRef)
    AudioFileOut("out", corrMax)
    //    MkDouble("out", corrMax)

    //    corrMax.ampDb.poll("corrMax [dB]")
    //e1.poll("e1")
    //e2.poll("e2")
  }
}
